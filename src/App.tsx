import { Component } from 'react';
import './App.css';
import Dashboard from './pages/Dashboard';

interface IProps {
  version: string 
}
interface IState {
  username: string
}

class App extends Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      username: "aprian"
    }
  }

  render() {
    return (
      <Dashboard
        username={this.state.username}
      />
    )
  }
}

export default App;
