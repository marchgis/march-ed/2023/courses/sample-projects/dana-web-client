import { Component } from "react";
import DashboardBottom from "./Bottom";
import DashboardContent from "./Content";
import DashboardHeader from "./Header";
import './Dashboard.css'

interface TipePropertinya {
  username: string
}
interface TipeStatenya {
}

class Dashboard extends Component<TipePropertinya, TipeStatenya> {
  render() {
    return (
      <div className="Dashboard">
        <DashboardHeader />
        <DashboardContent />
        <DashboardBottom />
      </div>
    )
  }
}

export default Dashboard
